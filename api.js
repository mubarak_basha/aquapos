const express = require('express')
const router = express.Router()
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const passport = require('passport')
const mongoose = require('mongoose')
const passportJWT = require('passport-jwt')

const ExtractJwt = passportJWT.ExtractJwt
const JwtStrategy = passportJWT.Strategy

const jwtOptions = {}
jwtOptions.jwtFromRequest = ExtractJwt.fromExtractors([ExtractJwt.fromBodyField('token'), ExtractJwt.fromUrlQueryParameter('token')])
jwtOptions.secretOrKey = 'AquaJalPos'

const strategy = new JwtStrategy(jwtOptions, async (jwtPayload, next) => {
  const user = await User.findById(jwtPayload.id)
  const shop = await Shop.findOne({user: user.id})
  if(user) {
    next(null, {user: user, shop: shop})
  } else {
    next(null, false)
  }
})

router.use(passport.initialize())
passport.use(strategy)

const User = mongoose.model('User')
const Shop = mongoose.model('Shop')
const Customer = mongoose.model('Customer')
const Product = mongoose.model('Product')
const Stock = mongoose.model('Stock')
const Sale = mongoose.model('Sale')
const Recoil = mongoose.model('Recoil')

router.post('/register', (req, res) => {
  const username = req.body.username
  const password = req.body.password
  const saltRounds = 10
  const hash = bcrypt.hashSync(password, saltRounds)
  const user = new User({
    username,
    password: hash
  })
  user.save()
    .then(dbUser => {
      console.log('dbUser', dbUser)
      return Shop.count({})
    })
    .then(count => {
      const shop = new Shop({
        name: `Shop ${(count+1)}`,
        address: '',
        phone: '',
        user: user._id
      })
      console.log('count', count)
      return shop.save()
    })
    .then(result => {
      res.json({success: true, message: 'user registered successfully!'})
    })
})

router.post('/shops', passport.authenticate('jwt', {session: false}) , async (req, res) => {
  const userId = req.user.id
  const name = req.body.name
  const address = req.body.address
  const phone = req.body.phone
  const shop = new Shop({
    name,
    address,
    phone,
    user: userId
  })
  await shop.save()
  res.json({success: true, message: 'shop created successfully!'})
})

router.post('/customers', passport.authenticate('jwt', {session: false}), async (req, res) => {
  const user = req.user.user
  const shop = req.user.shop
  const returnableProducts = req.body.returnableProducts.filter(product => product.qty > 0).map(product => {
    return {product: product.id, qty: product.qty}
  })
  const customer = new Customer({
    shop: shop.id,
    name: req.body.name,
    mobile: req.body.mobile,
    address: req.body.address,
    returnableProducts: returnableProducts
  })
  await customer.save()
  res.json({success: true, message: 'Customer saved successfully!'})
} )

router.get('/customers', passport.authenticate('jwt', {session: false}), (req, res) => {
  const shop = req.user.shop
  Customer.find({shop: shop.id})
    .populate({path: 'returnableProducts.product', model: 'Product', select: 'name'})
    .then(customers => res.json({success: true, customers: customers}))
    .catch(err => res.json({success: false, message: err}))
})

router.get('/customers/:id', passport.authenticate('jwt', {session: false}), (req, res) => {
  Customer.findById(req.params.id)
    .then((customer) => res.json({success: true, customer: customer}))
    .catch((err) => res.json({success: false, message: err}))
})

router.put('/customers/:id', passport.authenticate('jwt', {session: false}), (req, res) => {
  console.log(req.body.returnableProducts)
  const returnableProducts = req.body.returnableProducts.filter(product => product.qty > 0).map(product => {
    return {product: product.id, qty: product.qty}
  })
  console.log(returnableProducts)
  Customer.findByIdAndUpdate(req.params.id, {
    $set: {
      name: req.body.name, 
      mobile: req.body.mobile,
      address: req.body.address,
      returnableProducts: returnableProducts
    }
  })
  .then((cust) => res.json({success: true, message: 'Customer updated!'}))
  .catch((err) => res.json({success: false, message: err}))
})

router.delete('/customers/:id', passport.authenticate('jwt', {session: false}), (req, res) => {
  const shop = req.user.shop
  Customer.findOneAndDelete({shop: shop.id, _id: req.params.id})
    .then((cust) => res.json({success: true, message: 'Customer removed!'}))
    .catch((err) => res.json({success: false, message: err}))
})

router.post('/products', passport.authenticate('jwt', {session: false}), (req, res) => {
  const shop = req.user.shop

  const product = new Product({
    shop: shop.id,
    name: req.body.name,
    costPrice: req.body.costPrice,
    salesPrice: req.body.salesPrice,
    qty: req.body.qty,
    isReturnable: req.body.isReturnable
  })

  product.save()
    .then((prod) => res.json({success: true, message: 'Product saved successfully!'}))
    .catch((err) => res.json({success: false, message: err}))
  
})

router.get('/products', passport.authenticate('jwt', {session: false}), (req, res) => {
  const shop = req.user.shop
  Product.find({shop: shop.id})
    .then((products) => res.json({success: true, products: products}) )
    .catch((err) => res.json({success: false, message: err}))
})

router.get('/products/:id', passport.authenticate('jwt', {session: false}), (req, res) => {
  Product.findById(req.params.id)
    .then((product) => res.json({success: true, product: product}))
    .catch((err) => res.json({success: false, message: err}))
})

router.put('/products/:id', passport.authenticate('jwt', {session: false}), (req, res) => {
  Product.findByIdAndUpdate(req.params.id, {name: req.body.name,
    costPrice: req.body.costPrice,
    salesPrice: req.body.salesPrice,
    isReturnable: req.body.isReturnable})
    .then((prod) => res.json({success: true, message: 'Product updated!'}))
    .catch((err) => res.json({success: false, message: err}))
})

router.delete('/products/:id', passport.authenticate('jwt', {session: false}), (req, res) => {
  const shop = req.user.shop
  Product.findOneAndDelete({shop: shop.id, _id: req.params.id})
    .then((prod) => res.json({success: true, message: 'Product removed!'}))
    .catch((err) => res.json({success: false, message: err}))
})

router.get('/stocks/:id', passport.authenticate('jwt', {session: false}), (req, res) => {
  const id = req.params.id
  Stock.findById(id)
    .then((stock) => res.json({success: true, stock}))
    .catch((err) => res.json({success: false, message: err}))
})

router.put('/stocks/:id', passport.authenticate('jwt', {session: false}), async (req, res) => {

  const oldStock = await Stock.findById(req.params.id)
  if(oldStock) {

    oldStock.products.forEach(async (product) => {
      Product.findOneAndUpdate({_id: product.product}, {$inc: {qty: (-1 * product.qty) }})
        .catch((err) => console.log('error: ', err))
    })

    const products = req.body.products
    const stockProducts = products.map((product) => {
      return {product: product._id, costPrice: product.costPrice, salesPrice: product.salesPrice, qty: product.qty}
    })

    stockProducts.forEach(async (product) => {
      await Product.findOneAndUpdate({_id: product.product}, {$set: {costPrice: product.costPrice, salesPrice: product.salesPrice}, $inc: {qty: product.qty}})
    })

    Stock.findOneAndUpdate({_id: req.params.id}, {$set: {products: stockProducts} })
      .then((stockRes) => res.json({success: true, message: stockRes}))
      .catch((err) => res.json({success: false, message: err}))

  } else {

    res.json({success: false, message: 'Stock Not Found!'})
  }
  
})

router.get('/stocks', passport.authenticate('jwt', {session: false}), (req, res) => {
  Stock.aggregate([
    { $project: {
        invoiceNo: 1,
        productsCount: {$size: "$products"},
        createdOn: 1
      }
    },
    {
      $sort: {
        createdOn: -1
      }
    }
  ])
  .then((stocks) => res.json({success: true, stocks}))
  .catch((err) => res.json({success: false, message: err})) 
})

router.post('/stocks', passport.authenticate('jwt', {session: false}), async (req, res) => {
  const shop = req.user.shop
  const products = req.body.products
  const stockProducts = products.map((product)=> {
    return {product: product._id, costPrice: product.costPrice, salesPrice: product.salesPrice, qty: product.qty}
  })

  stockProducts.forEach(async (stockProduct) => {
    await Product.findOneAndUpdate({_id: stockProduct.product}, {$inc: {qty: stockProduct.qty}, $set: {costPrice: stockProduct.costPrice, salesPrice: stockProduct.salesPrice} })
  })

  let lastStockInvoiceNo = await Stock.findOne({shop: shop.id}, {invoiceNo: 1, _id: 0}).sort({createdOn: -1})
  if(!lastStockInvoiceNo){
    lastStockInvoiceNo = {invoiceNo: 0}
  }
  const stock = new Stock({
    invoiceNo: (lastStockInvoiceNo.invoiceNo+1),
    shop: shop.id,
    products: stockProducts
  })
  stock.save()
    .then((stk) => {

      console.log('new stock', stk)
      res.json({success: true, message: 'stock saved!', id: stk.id, invoiceNo: stk.invoiceNo})
    })
    .catch((err) => res.json({success: false, message: err}))
})

router.delete('/stocks/:id', passport.authenticate('jwt', {session: false}), (req, res) => {
  const id = req.params.id

  Stock.findById(id)
    .then((stock) => {
      stock.products.forEach(async (product) => {
        await Product.findOneAndUpdate({_id: product.product}, { $inc: {qty: (-1 * product.qty) } })
      })

      Stock.findOneAndRemove({_id: id})
        .then((dbRes) => res.json({success: true, message: dbRes}))
        .catch((err) => res.json({success: false, message: err}))

    })
    .catch((err) => res.json({success: false, message: err}))
  
})


router.get('/sales/:id', passport.authenticate('jwt', {session: false}), (req, res) => {
  const id = req.params.id
  Sale.findById(id)
    .then((sale) => res.json({success: true, sale}))
    .catch((err) => res.json({success: false, message: err}))
})

router.put('/sales/:id', passport.authenticate('jwt', {session: false}), async (req, res) => {
  const customerId = req.body.customer
  const products = req.body.products
  const totalAmount = req.body.totalAmount
  const paidAmount = req.body.paidAmount
  const balanceAmount = req.body.balanceAmount
  const newReturnableProducts = products.filter(product => product.isReturnable)

  //get customer
  const customer = await Customer.findOne({_id: customerId})
  //get oldsale
  const oldSale = await Sale.findOne({_id: req.params.id})
  //convert customer returnable product array to key (product id) value (qty) object for easy search.
  const customerProducts = {}
  customer.returnableProducts.forEach(prod => {
    customerProducts[prod.product] = prod.qty
  })

  //revert back the product's qty from old sale
  //update the object's qty by reducing it if it the product is found in old sale
  oldSale.products.forEach(oldProduct => {
    Product.findOneAndUpdate({_id: oldProduct.product}, {$inc: {qty: oldProduct.qty} })
    if(customerProducts[oldProduct.product]) {
      customerProducts[oldProduct.product] -=  parseFloat(oldProduct.qty)
    }
  })

  //update the objecty's qty if it is available in updated sale products.
  //or else add the product to the object.
  newReturnableProducts.forEach(prod => {
    if(customerProducts[prod._id]) {
      customerProducts[prod._id] = parseFloat(customerProducts[prod._id]) + parseFloat(prod.qty)
    } else {
      customerProducts[prod._id] = parseFloat(prod.qty)
    }
  })

  //convert object to array
  let nextCustomerProducts = []
  for(key in customerProducts) {
    if(customerProducts[key] > 0) {
      nextCustomerProducts.push({product: key, qty: customerProducts[key]})
    }
  }

  //set the array and save.
  customer.currentBalance = (parseFloat(customer.currentBalance) +  (parseFloat(balanceAmount) - parseFloat(oldSale.balanceAmount)))
  customer.returnableProducts = nextCustomerProducts
  customer.save()

  //update product and sales
  const saleProducts = products.map((product) => {
    return {product: product._id, costPrice: product.costPrice, salesPrice: product.salesPrice, qty: product.qty}
  })

  saleProducts.forEach(async (product) => {
    await Product.findOneAndUpdate({_id: product.product}, {$inc: {qty: (-1 * product.qty) }})
  })

  let payments = []
  if(paidAmount > 0){
    payments.push({date: Date.now, amount: paidAmount})
  }
  
  Sale.findOneAndUpdate({_id: req.params.id}, {$set: {products: saleProducts, customer: customerId, totalAmount: totalAmount, paidAmount: paidAmount, balanceAmount: balanceAmount, payments: []}})
    .then((saleRes) => res.json({success: true, message: 'Sales updated!'}))
    .catch((err) => res.json({success: false, message: err}))

})

router.get('/sales', passport.authenticate('jwt', {session: false}), (req, res) => {
  const shop = req.user.shop
  let promise = null
  let match = {shop: shop._id}
  if(req.query.type == 'credit'){
    match['balanceAmount'] = {$gt: 0}
  }
  Sale.aggregate([
    {
      $match: match
    },
    {
      $lookup: {
        from: 'customers',
        localField: 'customer',
        foreignField: '_id',
        as: 'customer'
      }
    },
    {
      $unwind: '$customer'
    },
    {
      $project: {
        invoiceNo: 1,
        productsCount: {$size: "$products"},
        totalAmount: 1,
        paidAmount: 1,
        balanceAmount: 1,
        payments: 1,
        createdOn: 1,
        customer: "$customer.name"
      }
    },
    {
      $sort: {createdOn: -1}
    }
  ])
    .then((sales) => res.json({success: true, sales}))
    .catch((err) => res.json({success: false, message: err}))
})

router.get('/customerSales', passport.authenticate('jwt', {session: false}), (req, res) => {
  let promise = null
  const shop = req.user.shop
  const customer = req.query.customer
  if(req.query.type == 'credit'){
    promise = Sale.aggregate([
      {
        $match: {
          shop: shop._id,
          customer: (new mongoose.Types.ObjectId(customer)),
          balanceAmount: {$gt: 0}
        }
      },
      {
        $project: {
          invoiceNo: 1,
          productsCount: {$size: "$products"},
          totalAmount: 1,
          paidAmount: 1,
          balanceAmount: 1,
          payments: 1,
          createdOn: 1
        }
      }
    ])

  }
  else {
    promise = Sale.aggregate([
      {
        $match: {
          shop: shop._id,
          customer: (new mongoose.Types.ObjectId(customer))
        }
      },
      {
        $project: {
          invoiceNo: 1,
          productsCount: {$size: "$products"},
          totalAmount: 1,
          paidAmount: 1,
          balanceAmount: 1,
          createdOn: 1,
        }
      },
      {
        $sort: {
          createdOn: -1
        }
      }
    ])
  }
  promise
    .then((sales) => res.json({success: true, sales}))
    .catch((err) => res.json({success: false, message: err}))
})

router.post('/sales', passport.authenticate('jwt', {session: false}), async (req, res) => {
  const shop = req.user.shop
  const customerId = req.body.customer
  const products = req.body.products
  const totalAmount = req.body.totalAmount
  const paidAmount = req.body.paidAmount
  const balanceAmount = req.body.balanceAmount

  let returnableProducts = []

  const saleProducts = products.map((product) => {
    if(product.isReturnable){
      returnableProducts.push({product: product._id, qty: product.qty})
    }
    return {product: product._id, costPrice: product.costPrice, salesPrice: product.salesPrice, qty: product.qty}
  })



  saleProducts.forEach(async (saleProduct) => {
    await Product.findOneAndUpdate({_id: saleProduct.product}, {$inc: {qty: (-1 * saleProduct.qty) }})
  })

  //update returnable products in customer
  const customer = await Customer.findById(customerId)
  let customerReturnableProducts = []
  returnableProducts.forEach(returnableProduct => {
    const rtnProd = {product: returnableProduct.product, qty: returnableProduct.qty}
    customer.returnableProducts.forEach(customerProduct => {
      if(String(customerProduct.product) == String(returnableProduct.product)) {
        rtnProd.qty = parseFloat(rtnProd.qty) + parseFloat(customerProduct.qty)
      }
    })
    customerReturnableProducts.push(rtnProd)
  })

  let finalReturnableProducts = []
  customer.returnableProducts.forEach(customerProduct => {
    let hasProduct = false
    customerReturnableProducts.forEach(returnableProduct => {
      if(returnableProduct.product == customerProduct.product){
        hasProduct = true
      }
    })
    if(!hasProduct){
      finalReturnableProducts.push(customerProduct)
    }
  })
  customer.currentBalance += parseFloat(balanceAmount)
  customer.returnableProducts = finalReturnableProducts.concat(customerReturnableProducts)
  await customer.save()

  let lastSaleInvoiceNo = await Sale.findOne({shop: shop.id}, {invoiceNo: 1, _id: 0}).sort({createdOn: -1})
  if(!lastSaleInvoiceNo){
    lastSaleInvoiceNo = {invoiceNo: 0}
  }

  const sale = new Sale({
    customer: customerId,
    invoiceNo: (lastSaleInvoiceNo.invoiceNo+1),
    shop: shop.id,
    products: saleProducts,
    totalAmount: totalAmount,
    paidAmount: paidAmount,
    balanceAmount: balanceAmount
  })
  
  if(parseFloat(paidAmount) > 0){
    sale.payments = [{date: Date.now(), amount: parseFloat(paidAmount)}]
  }

  sale.save()
    .then((sle) => {
      res.json({success: true, message: 'Sale Saved!', id: sle.id, invoiceNo: sle.invoiceNo})

    })
    .catch((err) => res.json({success: false, message: err}))
})

router.delete('/sales/:id', passport.authenticate('jwt', {session: false}), (req, res) => {
  const id = req.params.id
  Sale.findById(id)
    .then((sale) => {
      sale.products.forEach(product => {
        Product.findOneAndUpdate({_id: product.product}, {$inc: {qty: product.qty}})
      })
      Customer.findOne({_id: sale.customer}).then(customer => {
        customer.currentBalance -= sale.balanceAmount
        customer.save()
      })
      Sale.findOneAndRemove({_id: id})
        .then(dbRes => res.json({success: true, message: dbRes}))
        .catch(err => res.json({success: false, message: err}))
    })
    .catch(err => res.json({success: false, message: err}))
})

router.post('/emptyReturns', passport.authenticate('jwt', {session: false}), async (req, res) => {
  const shop = req.user.shop

  const data = req.body
  const customerId = data.customer
  const products = data.returnableProducts.map(product => ({product: product.id, qty: product.qty}))

  const customer = await Customer.findById(customerId, ['returnableProducts'])

  //update shop's empty products
  shopProducts = []
  customerProducts = []
  products.forEach(product => {
    let shopProd = {product: product.product, qty: product.qty}
    let customerProd = {product: product.product, qty: (-1 * product.qty)}
    shop.emptyProducts.forEach(shopProduct => {
      if(shopProduct.product == product.product) {
        shopProd.qty = parseFloat(shopProd.qty) + parseFloat(shopProduct.qty)
      }
    })

    customer.returnableProducts.forEach(customerProduct => {
      if(customerProduct.product == product.product) {
        customerProd.qty = parseFloat(customerProd.qty) + parseFloat(customerProduct.qty)
      }
    })

    shopProducts.push(shopProd)
    customerProducts.push(customerProd)
  })

  shop.emptyProducts = shopProducts
  await shop.save()

  customer.returnableProducts = customerProducts
  await customer.save()


  const recoil = new Recoil({
    customer: customerId,
    products: products
  })

  recoil.save()
    .then(dbRecoil => res.json({success: true, message: 'Empty returns saved!'}))
    .catch(err => res.json({success: false, message: err}))
})

router.get('/returnableProducts', passport.authenticate('jwt', {session: false}), (req, res) => {
  const shop = req.user.shop
  Product.find({shop: shop._id, isReturnable: true}, ["name"])
    .then(products => res.json({success: true, products}))
    .catch(err => res.json({success: false, message: err}))
})

router.post('/receipts', passport.authenticate('jwt', {session: false}), (req, res) => {
  const invoices = req.body.invoices
  let invoicesIds = []
  for(let key in invoices){
    invoicesIds.push(key)
  }

  Sale.find({_id: {$in: invoicesIds} })
    .then(sales => {
      sales.forEach(sale => {
        sale.paidAmount = (parseFloat(sale.paidAmount) + parseFloat(invoices[sale._id]))
        sale.balanceAmount = (parseFloat(sale.balanceAmount) - parseFloat(invoices[sale._id]))
        sale.payments.push({amount: invoices[sale._id]})
        sale.save()
        Customer.findOneAndUpdate({_id: sale.customer}, {$inc: {currentBalance: -parseFloat(invoices[sale._id]) } } )
          .then(res => console.log('res', res))
          .catch(err => console.log('err', err))
      });
    })
  
    res.send({success: true, message: 'Payment saved!'})

})

router.delete('/deletePayment/:saleId/', passport.authenticate('jwt', {session: false}), (req, res) => {
  const saleId = req.params.saleId
  const date = req.query.date
  const amount =req.query.amount
  Sale.findOneAndUpdate({_id: saleId}, {$inc: {paidAmount: (-1 * amount), balanceAmount: amount}, $pull: {"payments": {"date": date, amount: amount} } })
    .then(saleRes => {
      console.log(saleRes)
      Customer.findOneAndUpdate({_id: saleRes.customer}, {$inc: {currentBalance: parseFloat(amount) }})
      .then(customerRes => {
        console.log(customerRes)
        res.json({success: true, message: 'Payment removed successfully!'})
      })
      
    })
    .catch(err => res.json({success: false, message: err}))
})

router.get('/shop', passport.authenticate('jwt', {session: false}), (req, res) => {
  const user = req.user.user
  const shop = req.user.shop
  res.json({success: true, shop: shop})
})

router.put('/shop', passport.authenticate('jwt', {session: false}), (req, res) => {
  const shop = req.user.shop
  shop.name = req.body.name
  shop.address = req.body.address
  shop.phone = req.body.phone
  shop.emptyProducts = req.body.emptyProducts.map(product => {
    return {product: product.id, qty: product.qty}
  })
  shop.save()
    .then(response => res.json({success: true, message: 'Shop udpated Successfully!'}))
    .catch(err => res.json({success: false, message: err}))
})

router.get('/summary', passport.authenticate('jwt', {session: false}), (req, res) => {
  const shop = req.user.shop
  const date = new Date()
  const startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0)
  const endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59)
  Promise.all([
    Sale.aggregate([
      {
        $match: {
          createdOn: {$gte: startDate, $lte: endDate},
          shop: shop._id
        }
      },
      {
        $group: {
          _id: null,
          amount: {$sum: "$totalAmount"}
        }
      }
    ])
  ])
  .then(([saleResponse]) => {
    res.send({success: true, data: {todaySale: saleResponse.length > 0 ? saleResponse[0].amount : 0}})
  })
  .catch(err => {
    res.send({success: false, message: err})
  })
})

router.put('/updatePassword', passport.authenticate('jwt', {session: false}), (req, res) => {
  const user = req.user.user
  const password = req.body.password
  const saltRounds = 10
  const hash = bcrypt.hashSync(password, saltRounds)
  user.password = hash
  user.save()
    .then(response => res.json({success: true, message: 'Password updated successfully!'}))
    .catch(err => res.json({success: false, message: err}))
})

router.post('/authenticate', async (req, res) => {
  if(req.body.username.trim() == '' || req.body.password.trim() == '') {
    res.json({success: false, message: 'Username and Password required.'})
    return
  }

  User.findOne({username: req.body.username})
    .then(user => {
      if(!user){
        res.json({success: false, message: 'Invalid Username.'})
      } else if(user &&  !bcrypt.compareSync(req.body.password.trim(), user.password)){
        res.json({success: false, message: 'Invalid Password.'})
      } else {
        //in the future there may be many shops
        Shop.findOne({user: user.id})
          .then(shop => {
            const payLoad = {id: user.id, isAdmin: user.isAdmin, shop: shop}
            const token = jwt.sign(payLoad, jwtOptions.secretOrKey)
            res.json({success: true, token: token, shop: shop})
          })
      }
    })
    .catch(err => {
      res.json({success: false, message: 'some error' })
    })
})

module.exports = router