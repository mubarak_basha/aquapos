const mongoose = require('mongoose')
const Schema = mongoose.Schema

const stockSchema = new Schema({
  invoiceNo: Number,
  shop: {type: Schema.Types.ObjectId, ref: 'Shop'},
  products: [{product: {type: Schema.Types.ObjectId, ref: 'Product'}, costPrice: Number, salesPrice: Number, qty: Number}],
  createdOn: {type: Date, default: Date.now}
})

module.exports = mongoose.model('Stock', stockSchema)