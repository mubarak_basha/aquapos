const mongoose = require('mongoose')
const Schema = mongoose.Schema

const customerModel = new Schema({
  shop: {type: Schema.Types.ObjectId, ref: 'Shop'},
  name: String,
  mobile: String,
  address: String,
  createdOn: {type: Date, default: Date.now},
  currentBalance: {type: Number, default: 0},
  returnableProducts: [{product: {type: Schema.Types.ObjectId}, qty: Number}]
})

module.exports = mongoose.model('Customer', customerModel)