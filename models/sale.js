const mongoose = require('mongoose')
const Schema = mongoose.Schema

const saleSchema = new Schema({
  invoiceNo: Number,
  shop: {type: Schema.Types.ObjectId, ref: 'Shop'},
  customer: {type: Schema.Types.ObjectId, ref: 'Customer'},
  products: [{product: {type: Schema.Types.ObjectId, ref: 'Product'}, costPrice: Number, salesPrice: Number, qty: Number}],
  totalAmount: Number,
  paidAmount: Number,
  balanceAmount: Number,
  payments: [{date: {type: Date, default: Date.now}, amount: Number}],
  createdOn: {type: Date, default: Date.now}
})

module.exports = mongoose.model('Sale', saleSchema)