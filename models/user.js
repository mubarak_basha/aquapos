const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserModel = new Schema({
  name: String,
  email: {type: String},
  username: {type: String, unique: true},
  password: String,
  registeredOn: {type: Date, default: Date.now},
  recharges: [{date: Date, amount: Number}],
  regargeDue: Date,
  isAdmin: {type: Boolean, default: false}
})

module.exports = mongoose.model('User', UserModel)