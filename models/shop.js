const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ShopModel = new Schema({
  name: String,
  address: String,
  phone: String,
  user: {type: Schema.Types.ObjectId, ref: 'User'},
  createdOn: {type:Date, default: Date.now},
  emptyProducts: [{product: Schema.Types.ObjectId, qty: Number}]
})

module.exports = mongoose.model('Shop', ShopModel)