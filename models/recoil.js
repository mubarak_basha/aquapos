const mongoose = require('mongoose')
const Schema = mongoose.Schema

const recoilModel = new Schema({
  customer: {type: Schema.Types.ObjectId, ref: 'Customer'},
  products: [{product: {type: Schema.Types.ObjectId, ref: 'Product'}, qty: Number}],
  createdOn: {type: Date, default: Date.now}
})

module.exports = mongoose.model('Recoil', recoilModel)