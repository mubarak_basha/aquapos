const mongoose = require('mongoose')
const Schema = mongoose.Schema

const productSchema = new Schema({
  shop: {type: Schema.Types.ObjectId, ref: 'Shop'},
  name: {type: String, unique: true},
  costPrice: Number,
  salesPrice: Number,
  qty: {type: Number, default: 0},
  isReturnable: Boolean
})

module.exports = mongoose.model('Product', productSchema)