const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

mongoose.Promise = global.Promise

mongoose.connect('mongodb://mubarak:~Mubarak07@localhost:27017/aquajalpos', {useNewUrlParser: true}, (err)=> {
//mongoose.connect('mongodb://localhost:27017/aquajalpos', {useNewUrlParser: true}, (err)=> {
  if(err){
    console.log('error connecting to mongodb: ', err);
  }
  else {
    console.log('Connected to mongodb successfully!')
  }
})

const app = express()
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

//import models
require('./models/user')
require('./models/shop')
require('./models/customer')
require('./models/product')
require('./models/stock')
require('./models/sale')
require('./models/recoil')

app.use('/api', require('./api'))
//app.use(express.static(path.join(__dirname, '../material-test/dist')))
app.use(express.static('latest'))
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'latest', 'index.html'))
  //res.sendFile(path.join(__dirname, '../material-test/dist', 'index.html'))
})

app.listen(5000, (err)=> {
  if(err) {
    console.log('error starting node server', err)
  }else {
    console.log('node server started at 5000')
  }
})
